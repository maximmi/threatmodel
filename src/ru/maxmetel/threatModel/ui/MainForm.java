package ru.maxmetel.threatModel.ui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;

import ru.maxmetel.threatModel.core.Engine;
import ru.maxmetel.threatModel.entities.Protection;
import ru.maxmetel.threatModel.entities.Target;
import ru.maxmetel.threatModel.entities.Threat;
import ru.maxmetel.threatModel.factory.RandomFactories;
import ru.maxmetel.threatModel.ui.dialogs.Links;

import java.awt.BorderLayout;

import javax.swing.AbstractListModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class MainForm {

	private JFrame frame;
	private JList<Target> targets;
	private JList<Threat> threats;
	private JList<Protection> prots;
	private MainForm self = this;
	private List<Protection> pts = new ArrayList<>();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm window = new MainForm();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainForm() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 754, 480);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		DefaultListModel<Target> targetsModel = new DefaultListModel<>();
		targets = new JList<>(targetsModel);
		targets.setBounds(0, 0, 190, 343);
		targets.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		frame.getContentPane().add(targets);

		JButton btnGenTargets = new JButton("Generate");
		btnGenTargets.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				List<Target> tmp = RandomFactories.produceTargetsList(10);
				for (Target target : tmp) {
					targetsModel.addElement(target);
				}
			}
		});
		btnGenTargets.setBounds(0, 354, 190, 23);
		frame.getContentPane().add(btnGenTargets);

		DefaultListModel<Threat> threatModel = new DefaultListModel<>();
		threats = new JList<>(threatModel);
		threats.setBounds(275, 0, 190, 343);
		frame.getContentPane().add(threats);

		JButton btnAssignTT = new JButton("Assign");
		btnAssignTT.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Target target = targets.getSelectedValue();
				if (target == null) {
					JOptionPane.showMessageDialog(btnAssignTT, "Please select one target and some threats!");
				} else {
					List<Threat> th = target.getThreats();
					if (JOptionPane.showConfirmDialog(btnAssignTT, "Link " + target + " to "
							+ threats.getSelectedValuesList() + '?') == JOptionPane.YES_OPTION) {
						th.addAll(threats.getSelectedValuesList());
					}
				}
			}
		});
		btnAssignTT.setBounds(195, 185, 74, 23);
		frame.getContentPane().add(btnAssignTT);

		DefaultListModel<Protection> protsModel = new DefaultListModel<>();
		prots = new JList<>(protsModel);
		prots.setBounds(546, 0, 190, 343);
		prots.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		frame.getContentPane().add(prots);

		JButton btnAssignTTP = new JButton("Assign");
		btnAssignTTP.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Target target = targets.getSelectedValue();
				Threat threat = threats.getSelectedValue();
				Protection prot = prots.getSelectedValue();
				if (target == null || threat == null || prot == null) {
					JOptionPane.showMessageDialog(btnAssignTTP,
							"Select one target, threat and protection to create a link!");
				} else {
					if (JOptionPane.showConfirmDialog(btnAssignTTP,
							"Link " + prot + " to " + threat + " - " + target + "?") == JOptionPane.YES_OPTION) {
						prot.setTarget(target);
						prot.setThreat(threat);
						protsModel.remove(prots.getSelectedIndex());
					}
				}
			}
		});
		btnAssignTTP.setBounds(468, 185, 74, 23);
		frame.getContentPane().add(btnAssignTTP);

		JButton buttonGenThreats = new JButton("Generate");
		buttonGenThreats.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<Threat> tmp = RandomFactories.produceThreatsList(10);
				for (Threat threat : tmp) {
					threatModel.addElement(threat);
				}
			}
		});
		buttonGenThreats.setBounds(275, 354, 190, 23);
		frame.getContentPane().add(buttonGenThreats);

		JButton btnGenProts = new JButton("Generate");
		btnGenProts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<Protection> tmp = RandomFactories.produceProtectionList(10);
				for (Protection prot : tmp) {
					protsModel.addElement(prot);
					pts.add(prot);
				}
			}
		});
		btnGenProts.setBounds(546, 354, 190, 23);
		frame.getContentPane().add(btnGenProts);

		JButton btnAddTarget = new JButton("Add");
		btnAddTarget.setBounds(0, 385, 90, 23);
		frame.getContentPane().add(btnAddTarget);

		JButton btnLinks = new JButton("Links");
		btnLinks.setBounds(100, 385, 90, 23);
		btnLinks.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Target target = targets.getSelectedValue();
				if (target == null) {
					JOptionPane.showMessageDialog(btnAssignTT, "Please select one target and some threats!");
				} else {
					Links links = new Links(pts, target);
					links.setVisible(true);
				}
			}
		});
		frame.getContentPane().add(btnLinks);

		JButton btnAddThreat = new JButton("Add");
		btnAddThreat.setBounds(275, 385, 190, 23);
		frame.getContentPane().add(btnAddThreat);

		JButton btnAddProtection = new JButton("Add");
		btnAddProtection.setBounds(546, 385, 190, 23);
		frame.getContentPane().add(btnAddProtection);

		JButton btnFire = new JButton("PWN!");
		btnFire.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int NUM_DAYS = 3;
				StringBuilder out = new StringBuilder();
				Engine engine = new Engine(targetsModel, pts);
				List<List<Integer>> bare = engine.pwn(NUM_DAYS, false);
				List<Integer> sums = engine.getTotalPrices();
				out.append("Without protection\n");
				out.append(bare);
				out.append('\n');
				List<List<Integer>> wProtect = engine.pwn(NUM_DAYS, true);
				List<Integer> protSums = engine.getTotalPrices();
				int protCost = engine.getProtectionCost();
				out.append("With protection\n");
				out.append(wProtect);
				out.append('\n');
				List<Integer> deltas = new ArrayList<>(NUM_DAYS);
				for (int i = 0; i < NUM_DAYS; i++) {
					deltas.add(protSums.get(i) - sums.get(i) - protCost);
				}
				out.append("Leftovers without protection: " + sums + "\n with: "
						+ protSums + " deltas: " + deltas);
				JOptionPane.showMessageDialog(btnFire, out.toString(), "Simulation log", 
						JOptionPane.INFORMATION_MESSAGE);
			}
		});
		btnFire.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnFire.setBounds(0, 419, 738, 23);
		frame.getContentPane().add(btnFire);
	}
}
