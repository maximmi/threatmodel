package ru.maxmetel.threatModel.ui.dialogs;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import ru.maxmetel.threatModel.entities.Protection;
import ru.maxmetel.threatModel.entities.Target;
import ru.maxmetel.threatModel.entities.Threat;
import ru.maxmetel.threatModel.factory.RandomFactories;
import ru.maxmetel.threatModel.ui.MainForm;

import javax.swing.JList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

public class Links extends JDialog {

	private Links self = this;
	private final JPanel contentPanel = new JPanel();
	private JList<Threat> threats = new JList<>();
	private JList<Protection> prots = new JList<>();
	private Target target;
	private List<Protection> allProts;
	private DefaultListModel<Threat> threatsModel = new DefaultListModel<>();
	private DefaultListModel<Protection> protsModel = new DefaultListModel<>();
	/**
	 * Create the dialog.
	 */
	@Deprecated
	public Links() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		threats.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) 
			{
				protsModel.clear();
				Threat threat = threats.getSelectedValue();
				
				for (int i=0; i<allProts.size(); i++) {
					Protection prot = allProts.get(i);
					if (prot.getThreat() != null && prot.getTarget() != null && prot.getTarget().equals(target) && prot.getThreat().equals(threat)) {
						protsModel.addElement(prot);
					}
				}
				prots.setModel(protsModel);
			}
		});
		
		threats.setBounds(0, 0, 185, 186);
		threats.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		contentPanel.add(threats);
		
		prots.setBounds(240, 0, 185, 186);
		prots.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		contentPanel.add(prots);
		
		JButton btnDelThreat = new JButton("Del");
		btnDelThreat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = threats.getSelectedIndex();
				target.getThreats().remove(i);
				threatsModel.remove(i);
			}
		});
		btnDelThreat.setBounds(97, 195, 89, 23);
		contentPanel.add(btnDelThreat);
		
		JButton btnDelProtection = new JButton("Del");
		btnDelProtection.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = prots.getSelectedIndex();
				prots.getSelectedValue().setTarget(null);
				prots.getSelectedValue().setThreat(null);
				protsModel.remove(i);
			}
		});
		btnDelProtection.setBounds(335, 197, 89, 23);
		contentPanel.add(btnDelProtection);
		
		
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Close");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						self.dispose();
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
	}
	
	public Links(List<Protection> prots, Target target) {
		this();
		this.allProts = prots;
		this.target = target;
		List<Threat> tmp = target.getThreats();
		for (Threat threat : tmp) {
			threatsModel.addElement(threat);
		}
		threats.setModel(threatsModel);
	}
}
