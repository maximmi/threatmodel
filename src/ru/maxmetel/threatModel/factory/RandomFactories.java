package ru.maxmetel.threatModel.factory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ru.maxmetel.threatModel.entities.Protection;
import ru.maxmetel.threatModel.entities.Target;
import ru.maxmetel.threatModel.entities.Threat;

public class RandomFactories {
	private static final int MAXIMAL_PROTECTION_COST = 50;
	private static final int MAXIMAL_UNIT_COST = 1000;

	public static List<Protection> produceProtectionList(int number) {
		Random random = new Random();
		List<Protection> result = new ArrayList<Protection>(number);

		for (int i = 0; i < number; i++) {
			result.add(new Protection(Math.abs(random.nextInt(MAXIMAL_PROTECTION_COST)), random.nextFloat(),
					random.nextFloat(), null, null));
		}

		return result;
	}

	public static List<Threat> produceThreatsList(int number) {
		Random random = new Random();
		List<Threat> result = new ArrayList<Threat>(number);

		for (int i = 0; i < number; i++) {
			result.add(new Threat(random.nextFloat(), random.nextFloat()));
		}

		return result;
	}

	public static List<Target> produceTargetsList(int number) {
		Random random = new Random();
		List<Target> result = new ArrayList<>(number);

		for (int i = 0; i < number; i++) {
			result.add(new Target((i + 1), Math.abs(random.nextInt(MAXIMAL_UNIT_COST))));
		}

		return result;
	}
}
