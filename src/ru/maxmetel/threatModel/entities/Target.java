package ru.maxmetel.threatModel.entities;

import java.util.ArrayList;
import java.util.List;

public class Target {
	private int n;
	private int value, defaultValue;
	private List<Threat> threats;
	public Target(int n, int value) {
		super();
		this.n = n;
		this.value = value;
		this.defaultValue = value;
		this.threats = new ArrayList<>();
	}
	public int getN() {
		return n;
	}
	public void setN(int n) {
		this.n = n;
	}
	public int getValue() {
		return value;
	}
	public List<Threat> getThreats() {
		return threats;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public void setThreats(List<Threat> threats) {
		this.threats = threats;
	}
	public void pwnBy(float pwnness) {
		this.value *= 1.0-pwnness;
	}
	public void restore() {
		this.value = this.defaultValue;
	}
	@Override
	public String toString() {
		return String.format("Target %s [value=%s]", n, value);
	}
	
}
