package ru.maxmetel.threatModel.entities;

public class Protection {
	private int price;
	private float eff;
	private float probability;
	private Threat threat;
	private Target target;
	
	public Protection(int price, float eff, float probability, Threat threat, Target target) {
		super();
		this.price = price;
		this.eff = eff;
		this.probability = probability;
		this.threat = threat;
		this.target = target;
	}
	public int getPrice() {
		return price;
	}
	public float getEff() {
		return eff;
	}
	public Target getTarget() {
		return target;
	}
	public float getProbability() {
		return probability;
	}
	public Threat getThreat() {
		return threat;
	}
	public void setThreat(Threat threat) {
		this.threat = threat;
	}
	public void setTarget(Target target) {
		this.target = target;
	}
	@Override
	public String toString() {
		return String.format("Prot [cost=%s, eff=%.2f, p=%.2f]", price, eff, probability);
	}
}
