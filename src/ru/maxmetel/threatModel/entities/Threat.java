package ru.maxmetel.threatModel.entities;

public class Threat {
	private float damage;
	private float probability;
	public Threat(float damage, float probability) {
		super();
		this.damage = damage;
		this.probability = probability;
	}
	public float getDamage() {
		return damage;
	}
	public float getProbability() {
		return probability;
	}
	@Override
	public String toString() {
		return String.format("Threat [damage=%.2f, p=%.2f]", damage, probability);
	}
	
}
