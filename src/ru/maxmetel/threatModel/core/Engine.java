package ru.maxmetel.threatModel.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.DefaultListModel;

import ru.maxmetel.threatModel.entities.Protection;
import ru.maxmetel.threatModel.entities.Target;
import ru.maxmetel.threatModel.entities.Threat;

public class Engine {

	private DefaultListModel<Target> targets;
	private List<Protection> allProts;
	private DiceRoll dice = new DiceRoll();
	private Random rand = new Random();
	private int protectionCost = 0;
	private List<Integer> sums = new ArrayList<>();

	public Engine(DefaultListModel<Target> targetsModel, List<Protection> pts) {
		this.targets = targetsModel;
		this.allProts = pts;
	}
	
	public List<Integer> pwnStep(boolean protect) {
		List<Integer> prices = new ArrayList<>();
		int sum = 0;
		this.protectionCost = 0;
		for (int i=0; i<targets.size(); i++) {
			Target target = targets.getElementAt(i);
			List<Threat> threats = target.getThreats();
			for (Threat threat : threats) {
				float damage = threat.getDamage();
				if (protect) {
					for (Protection prot : allProts) {
						if (prot.getThreat() != null && prot.getTarget() != null && prot.getTarget().equals(target) && prot.getThreat().equals(threat)) {
							if (rand.nextFloat() < prot.getProbability()) damage *= 1-prot.getEff();
							this.protectionCost += prot.getPrice();
						}
					}
				}
				
				if (dice.getNext() < threat.getProbability()) {
					target.pwnBy(damage);
				}
			}
			prices.add(target.getValue());
			sum += target.getValue();
		}
		this.sums.add(sum);
		return prices;
	}
	
	public int getProtectionCost() {
		return this.protectionCost;
	}
	
	public List<Integer> getTotalPrices() {
		//return this.sums; //that's fucking LINK!
		List<Integer> ret = new ArrayList<>(this.sums.size());
		ret.addAll(this.sums);
		return ret;
	}
	
	public List<List<Integer>> pwn(int numSteps, boolean protect) {
		List<List<Integer>> pwnageLog = new ArrayList<>();
		dice.reset();
		sums.clear();
		for (int i=0; i<targets.size(); i++) {
			targets.getElementAt(i).restore();
		}
		for (int i=0; i<numSteps; i++) {
			pwnageLog.add(pwnStep(protect));
		}
		return pwnageLog;
	}
}
