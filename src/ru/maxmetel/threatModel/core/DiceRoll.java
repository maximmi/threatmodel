package ru.maxmetel.threatModel.core;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class DiceRoll {
    private final int SEQUENCE_SIZE = 65_535;

    private List<Float> numbers;
    private int position = 0;

    public DiceRoll() {
        throwDice();
    }

    private void throwDice() {
        SecureRandom random = new SecureRandom();
        numbers = new ArrayList<>(SEQUENCE_SIZE);

        for (int i = 0; i < SEQUENCE_SIZE; i++) {
            numbers.add(random.nextFloat());
        }
    }

    public float getByPosition(int position) {
        return numbers.get(position % SEQUENCE_SIZE);
    }
    
    public float getNext() {
    	 float x = numbers.get(this.position % SEQUENCE_SIZE);
    	 this.position++;
    	 return x;
    }
    
    public void reset() {
    	this.position = 0;
    }
}
